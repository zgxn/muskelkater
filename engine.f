!
! module : engine.f
! -----------------
!
! implementation of MUSCL-Hancock algorithm
! cf. Toro, E.F. (2009), Riemann solvers and 
!     Numerical Methods for Fluid Dynamics.
!
! information on copyright and licensing can be found 
! in LICENSE.
!
! author: ilhan oezgen, wahyd, 26 sep 2016
!                               6 oct 2016
!
module engine

use param, only: dp, header, footer, eps
use methods, only: get_edge_vals, riemann_flux, update

contains




!
! solve the equations using MUSCL Hancock algorithm
!
subroutine run(n, dx, q_init, dt_init, t_end)

implicit none

integer, intent(in) :: n

real(dp), intent(in) :: dx
real(dp), intent(in) :: dt_init
real(dp), intent(in) :: t_end

real(dp), intent(inout) :: q_init(n,3)

real(dp) :: t, dt, dtn

real(dp) :: qn(n,3)
real(dp) :: wl(n,3)
real(dp) :: wr(n,3)
real(dp) :: f(n+1,3)

integer :: co

wl(:,:) = 0.0
wr(:,:) = 0.0

co = 0
t = 0.0
dt = dt_init
qn = q_init


call header() ! write something on the console
write (*,*) "*************"
write (*,*) "MUSCL-HANCOCK"
write (*,*) "*************"

do while (t .lt. t_end)

  dtn = 1.0_dp ! maximum allowable time step

  call get_edge_vals(n, q_init, wl, wr, dt, dx)
  call riemann_flux(n, wl, wr, f)
  call update(n, q_init, qn, f, dt, dtn, dx)

  q_init = qn
  co = co + 1

  if (mod(co, 100) .eq. 0) then
    write (*,*) "steps : ", co, ", time : ", t, " s, dt : ", dt, " s."
  end if

  t = t + dt
  dt = dtn

end do

call footer(t)

end subroutine run


!
!



!
! solve the equations using first order Godunov algorithm.
! implemented for reference purposes.
!
subroutine first_order_godunov(n, dx, q_init, dt_init, t_end)

implicit none

integer,  intent(in) :: n
real(dp), intent(in) :: dx
real(dp), intent(in) :: dt_init
real(dp), intent(in) :: t_end

real(dp), intent(inout) :: q_init(n,3)

real(dp) :: t         ! time
real(dp) :: qn(n,  3) ! storage vector at new time step
real(dp) ::  f(n+1,3) ! flux vector
real(dp) :: dt, dtn
integer  :: co


qn = q_init
dt = dt_init
co = 0
t  = 0.0


call header()

write (*,*) "*******************"
write (*,*) "FIRST ORDER GODUNOV"
write (*,*) "*******************"

do while (t .lt. t_end)

  dtn = 1.0_dp

  call riemann_flux(n, q_init, q_init, f)
  call update(n, q_init, qn, f, dt, dtn, dx)

  if (mod(co, 100) .eq. 0) then
    write (*,*) "steps : ", co, ", time : ", t, " s, dt : ", dt, " s."
  end if

  dt = dtn

  q_init = qn
  co     = co + 1
  t      = t  + dt

end do

call footer(t)

end subroutine first_order_godunov



end module engine
