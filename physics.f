!
! module : physics.f
! ------------------
!
! definition of flux vector for shallow water equations 
! with bottom topography in one dimension.
!
! information on copyright and licensing can be found 
! in LICENSE.
!
!
! author: ilhan oezgen, wahyd, 26 sep 2016
!                               6 oct 2016
!
!
module physics

use param, only: dp, eps, g




contains


!
! gives the flux vector
!   | q = [ h, q, z ]
!   | e = h + z
!
subroutine f(q, flux)

implicit none

real(dp), intent(in)  :: q(3)
real(dp), intent(out) :: flux(3)

flux(:) = 0.0_dp

if (q(1) .gt. eps) then
  flux(1) = q(2)
  flux(2) = (q(2)**2 / q(1)) + 0.5 * g * q(1)**2
end if

end subroutine f



end module physics
