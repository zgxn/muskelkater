!
! program : main.f
! ----------------
!
! steering file
!
! information on copyright and licensing can be found 
! in LICENSE.
!
! author: ilhan oezgen, wahyd, 23 sep 2016
!                              28 sep 2016
!                               6 oct 2016
!
program main


use param, only: dp
use engine, only: run

implicit none

integer :: u
integer :: i, n

character(len=30) :: in_file
character(len=30) :: out_file

real(dp) :: dx, t_end
real(dp), allocatable :: q(:,:)
real(dp), allocatable :: x(:)

write(*, *) "provide steering file"
read (*, *) in_file

open(newunit=u, file=in_file, status="old")
read(u, *) n, dx, t_end

allocate( x(n) )
allocate( q(n,3) )

!
! read data from file
!
do i = 1, n
  read(u, *) x(i), q(i,1), q(i,2), q(i,3)
end do

read(u, *) out_file

close(u)


!
! run simulation
!
call run(n, dx, q, 1.0e-4_dp, t_end)



!
! write result to file
!
open(newunit=u, file=out_file)
do i = 1, n
  write(u, *) x(i), ", ", q(i,1), ", ", q(i,2), ", ", q(i,3)
end do

close(u)



end program main
