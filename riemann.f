!
! module : riemann.f
! ------------------
!
! provides the hll riemann solver
! cf. Toro, E.F. (2001), Shock-Capturing Methods for 
!     Free-Surface Shallow Flows
!
! author: ilhan oezgen, wahyd, 26 sep 2016
!                               6 oct 2016
!


module riemann

use param, only: dp, g, eps
use physics, only: f

contains





subroutine hll_flux(wl, wr, flux)

implicit none

real(dp), intent(in) :: wl(3)
real(dp), intent(in) :: wr(3)

real(dp), intent(out) :: flux(3)

real(dp) :: fl(3)
real(dp) :: fr(3)
real(dp) :: hl, hr, hm
real(dp) :: vl, vr
real(dp) :: sl, sr ! wave speed estimates
real(dp) :: al, ar ! wave speed of water
real(dp) :: ql, qr ! coefficient

hl = wl(1)
hr = wr(1)

if (hl .gt. eps) then
  vl = wl(2) / hl
else
  vl = 0.0
end if

if (hr .gt. eps) then
  vr = wr(2) / hr
else
  vr = 0.0
end if

al = sqrt(g * hl)
ar = sqrt(g * hr)

! calculate left and right fluxes
call f(wl, fl)
call f(wr, fr)

!
!
! calculate the wave speeds
!
if ( (hl .lt. eps) .and. (hr .lt. eps) ) then

  ! dry bed on both sides
  flux(:) = 0.0_dp

else

  hm = (1.0 / g) * ( 0.5 * (al + ar) &
  + 0.25 * ( vl - vr ) )**2  ! middle state estimate

  if (hl .lt. eps) then ! left side is dry

    sl = vr - 2.0 * ar

  else

    call qk(hm, hl, ql)
    sl = vl - al * ql

  end if

  if (hr .lt. eps) then ! right side is dry

    sr = vl - 2.0 * al

  else

    call qk(hm, hr, qr)
    sr = hr + 2.0 * ar

  end if

end if



!
!
! calculate the flux

if ( sl .gt. 0.0 ) then
  flux = fl
else if ( (sl .lt. 0.0) .and. (sr .gt. 0.0) ) then
  flux = (1.0 / (sr - sl)) * ( sr * fl - sl * fr + sr * sl * (wr - wl) )
else
  flux = fr
end if

end subroutine hll_flux



!
! calculate the coefficient qk
!
subroutine qk(hm, hk, q)

implicit none

real(dp), intent(in) :: hm
real(dp), intent(in) :: hk

real(dp), intent(out) :: q


if ( hk .lt. hm ) then
  q = sqrt( 0.5 * ( (hm + hk) * hm ) / hk**2 )
else
  q = 1.0
end if

end subroutine qk

end module riemann
