Tested with gfortran 4.8.4 on Ubuntu 14.04. 
Tested with gfortran 4.9.2 on Debian  8.6.

See Makefile for compilation options.

Written in Fortran 90.
Information about copyright and licensing can be found in LICENSE.

(c) Ilhan Ozgen, www.bosai.riken.jp
