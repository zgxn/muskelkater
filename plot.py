#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np

a = np.genfromtxt('RES/analytical.csv', delimiter=',')
q = np.genfromtxt('RES/godunov.csv', delimiter=',')
m = np.genfromtxt('RES/muscl_hancock.csv', delimiter=',')

plt.plot(a[:,0], a[:,1], 'k-', label='analytical')
plt.plot(q[:,0], q[:,1], 'ko', fillstyle='none', label='Godunov')
plt.plot(m[:,0], m[:,1], 'ro', fillstyle='none', label='MUSCL-Hancock')

plt.ylim(0.0, 4.5)
plt.xlabel('x (m)')
plt.ylabel('h (m)')
plt.title('t = 0.1 s')

plt.legend()

plt.show()
