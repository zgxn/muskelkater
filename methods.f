!
! module : methods.f
! ------------------
!
! collection of numerical methods required for 
! MUSCL-type schemes
!
! information on copyright and licensing can be found 
! in LICENSE.
!
! author: ilhan oezgen, wahyd, 23 sep 2016
!
module methods

use param, only: dp, cfl, eps, g
use physics, only: f
use riemann, only: hll_flux



contains



!
! calculate slope in one cell with a three point stencil
!
subroutine get_slope(wp, w, wn, sl)

implicit none

real(dp), intent(in) :: wp
real(dp), intent(in) :: w
real(dp), intent(in) :: wn

real(dp), intent(out) :: sl

real(dp) :: a
real(dp) :: b

a = w - wp
b = wn - w

call min_mod(a, b, sl)

end subroutine get_slope





!
!
! min-mod limiter function
!
subroutine min_mod(x, y, sl)

implicit none

real(dp), intent(in) :: x
real(dp), intent(in) :: y

real(dp), intent(out) :: sl

if (x * y .le. 0.0) then
  sl = 0.0
else if (abs(x) .lt. abs(y)) then
  sl = x
else
  sl = y
end if

end subroutine min_mod




!
!
!
! reconstruct the values at the edge
!
subroutine get_edge_vals(n, q0, wl, wr, dt, dx)

implicit none

integer, intent(in) :: n

real(dp), intent(in) :: q0(n,3)
real(dp), intent(in) :: dt
real(dp), intent(in) :: dx

real(dp), intent(out) :: wl(n,3)
real(dp), intent(out) :: wr(n,3)

real(dp) :: sl(n,3) ! slope
real(dp) :: fl(n,3) ! flux left
real(dp) :: fr(n,3) ! flux right

real(dp) ::  e(n) ! water elevation

integer :: i

! setting the boundary values

wl(1,:) = q0(1,:)
wr(1,:) = q0(1,:)

wl(n,:) = q0(n,:)
wr(n,:) = q0(n,:)

e = q0(:,1) + q0(:,3) ! water elevation is h + z

do i = 2, (n-1)

  call get_slope(q0(i-1,1), q0(i,1), q0(i+1,1), sl(i,1)) ! depth
  call get_slope(q0(i-1,2), q0(i,2), q0(i+1,2), sl(i,2)) ! discharge
  call get_slope( e(i-1),    e(i),    e(i+1)  , sl(i,3)) ! elevation

  wl(i,1:2) = q0(i,1:2) + 0.5 * sl(i,1:2)
  wr(i,1:2) = q0(i,1:2) - 0.5 * sl(i,1:2)


  !
  ! bottom elevation is calculated as the difference 
  ! between water elevation and water depth
  ! /
  ! cf. Hou et al. (2013), A 2D well-balanced shallow 
  ! flow model for unstructured grids with novel 
  ! slope source term treatment, Adv Wat Res 52, 107-131.
  ! /
  !
  !
  wl(i,3) = e(i) + 0.5 * sl(i,3) - wl(i,1)
  wr(i,3) = e(i) - 0.5 * sl(i,3) - wr(i,1)
  !
  !
  !


  if (wl(i,3) .lt. eps) then
    wl(i,3) = 0.0_dp
  end if

  if (wr(i,3) .lt. eps) then
    wr(i,3) = 0.0_dp
  end if


  ! advance in time
  call f(wl(i,:), fl(i,:))
  call f(wr(i,:), fr(i,:))

  wl(i,:) = wl(i,:) - 0.5 * (dt/dx) * (fl(i,:) - fr(i,:))
  wr(i,:) = wr(i,:) - 0.5 * (dt/dx) * (fl(i,:) - fr(i,:))

end do

end subroutine get_edge_vals




!
! calculate riemann flux
!
subroutine riemann_flux(m, wl, wr, f)

implicit none

integer,  intent(in) :: m
real(dp), intent(in) :: wl(m,2)
real(dp), intent(in) :: wr(m,2)

real(dp), intent(out) :: f((m+1),2)

integer :: i

do i = 2, m
  call hll_flux(wl(i-1,:), wr(i,:), f(i,:))
end do

end subroutine riemann_flux







!
! update the storage vector using finite volume formulation
!
subroutine update(n, q0, qn, f, dt, dtn, dx)

implicit none

integer,  intent(in) :: n
real(dp), intent(in) :: q0(n,  2)
real(dp), intent(in) ::  f(n+1,2)
real(dp), intent(in) :: dx
real(dp), intent(in) :: dt

real(dp), intent(inout) :: dtn
real(dp), intent(out) :: qn(n,2)

integer :: i

qn = q0

do i = 2, (n-1)
  qn(i,:) = q0(i,:) - (dt/dx) * (f(i+1,:) - f(i,:))
  call get_new_timestep(qn(i,:), dtn, dx) ! calculate new time step
end do

end subroutine update




!
! calculate the new time step
!
subroutine get_new_timestep(qn, dt, dx)

real(dp), intent(in) :: qn(2)
real(dp), intent(in) :: dx
real(dp), intent(inout) :: dt

real(dp) :: h, v

h = qn(1)
v = 0.0_dp

if (h .gt. eps) then
  v = abs(qn(2) / h)
  dt = min(dt, cfl * dx / (v + sqrt(g * h)))
end if

end subroutine get_new_timestep



end module methods
