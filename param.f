!
! param.f
! -------
!
! numerical parameters for the model
!
! author: ilhan oezgen, wahyd, 23 sep 2016

module param


public
integer, parameter  :: dp = kind(0.d0) ! double precision
real(dp), parameter :: cfl = 0.8
real(dp), parameter :: eps = 1.0e-7
real(dp), parameter :: g = 9.81


contains


!
! write out the header to indicate that the simulation has started
!
subroutine header()

implicit none

write (*,*) "---------------------------------------------------"
write (*,*) "____________  M U S K E L K A T E R  ____________  "
write (*,*) "---------------------------------------------------"

end subroutine header

!
! write out the footer to indicate that the simulation has finished
! param :
! t [in] : current model time
!
subroutine footer(t)

implicit none

real(dp), intent(in) :: t

write (*,*) "---------------------------------------------------"
write (*,*) "simulation finished at t : ", t, " s."
write (*,*) "---------------------------------------------------"

end subroutine footer

end module param
