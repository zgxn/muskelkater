#
# Compile the .f-files and create an executable
#
# author : ilhan oezgen, wahyd, 23 sep 2016

FC = f95

FFLAGS = -Wall -pedantic -ffree-form
SWITCH = -O0

SOURCES = param.f physics.f riemann.f methods.f engine.f
OBJECTS = param.o physics.o riemann.o methods.o engine.o

.PHONY = clean

all: $(SOURCES)
	make objects
	make main

main: $(OBJECTS)
	$(FC) $(FFLAGS) $(SWITCH) main.f $(OBJECTS) -o main

objects: $(SOURCES)
	$(FC) $(FFLAGS) -c $(SWITCH) $(SOURCES)

clean:
	rm -f $(OBJECTS) *.mod main
